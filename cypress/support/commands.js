// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import 'cypress-file-upload';
import Locators from '../integration/PageObjects/Locators.json';
Cypress.Commands.add("AuthorizeBGP", (username, password) => { 
    let username1 = "public"
    let password1 = "Let$BeC001"
    cy.visit({
      url: 'https://bgp-qa.gds-gov.tech/',
      auth: {
        username: username1,
        password: password1
      }  
    })
})

Cypress.Commands.add("logintoBGP", (CPUID, CPUID_Fullname,CPENT_ID) => {
    cy.get('[id="login-button"]').click()
    cy.get('[name="CPUID"]').clear().type(CPUID)
    cy.get('[name="CPUID_FullName"]').clear().type(CPUID_Fullname)
    cy.get('[name="CPEntID"]').clear().type(CPENT_ID)
    cy.get('[type="submit"]').last().click()
 })

 Cypress.Commands.add("FindProfile",() => {
  cy.get(Locators.My_Grants , {timeout: 5000 }).first().click()
  cy.get(Locators.Draft_Grants).click()
  cy.get(Locators.Draft_Project_Title).contains('Construct Hospital').click()
  cy.get(Locators.Proceed).click()
 })

 Cypress.Commands.add("GetNewGrant",() => {
  cy.get(Locators.New_Grant,{ timeout: 20000 }).click()
  cy.get(Locators.Select_Sector_IT).click()
  cy.get(Locators.Choose_International_Expansion).click()
  cy.get(Locators.Choose_Market_Readiness).click()
  cy.get(Locators.Apply_Grant).click()
  cy.get(Locators.Proceed).click()
 })

 Cypress.Commands.add("CreateSampleGrant",() => {
  cy.get(Locators.Proposal).click()
  cy.get(Locators.Project_Title).first().type('Construct Hospital')  //Project Title
  cy.get(Locators.SaveBtn).click()
 })



