import Locators from '../PageObjects/Locators.json';
Cypress.config({'pageLoadTimeout':100000,
                'defaultCommandTimeout':15000})
                describe('Test Business Grants Portal - New Grant Automation Flow', () => {

                  beforeEach('Testing the Window Prompt with Credentials', () => {
                    
                    // Created Customized Command for Authorize and Login
                    cy.AuthorizeBGP(Locators.Authorize_username,Locators.Authorize_password)
                    cy.logintoBGP(Locators.CPUID,Locators.CPUID_Fullname,Locators.CPENT_ID)
                
                    // Verified the Title of the Page (Functional Testing)
                    cy.title().should('eq', 'Business Grants Portal')
                    cy.get(Locators.My_Grants).first().click()
                  });

  it('Create a New Grant - Construct Hospital and School', () => {

    //Get a New Grant
    cy.get('#dashboard-menubox-app-apply-grant > .col-sm-12 > .dashboard-action-card > .dashboard-action-text-wrapper > .dashboard-action-title').click()
    cy.get(Locators.Select_Sector_IT).click()
    cy.get('input[id="International Expansion"]').click()
    cy.get('input[id="Market Readiness Assistance 2"').click()
    cy.get(Locators.Apply_Grant).click()
    cy.get(Locators.Proceed).click()

    // Here create a sample Grant,which can get in Drafts
    cy.get('a[href$="/project"]').click()
    cy.get('#react-project-title').first().type('Construct Hospital and School')  //Project Title
    cy.get('#save-btn').click()


  })

  it.skip('Verify the Grant from Draft and Test Acess Crtiterias', () => {
    //Verify Grant has been created in the Drafy for performing the user stories
    cy.get(Locators.My_Grants).first().click()
   
    cy.get('a[href$="#drafts"]').click()
    
    cy.get('#db-apps-drafts > .tab-content > :nth-child(1) > .project-title > a > .title-div').click()

  })
  /*
  AC 2: If there are any mandatory inputs missing, a validation error should trigger and 
  the form should redirect to the section with the missing details. 
  An error number should also be shown in the sidebar next to the offending section. 
  */
  it('UserStory 3: AC2', () => {

    cy.get(Locators.My_Grants).first().click()
  
    cy.get('a[href$="#drafts"]').click()
    
    cy.get('#db-apps-drafts > .tab-content > :nth-child(1) > .project-title > a > .title-div').click()
    cy.get(Locators.Proceed).click()
   
    cy.get('a[href$="declaration"]').click()
    cy.get(Locators.ReviewBtn).click()
    cy.get(Locators.Mandatory_Label_Error).then(($error) => {
      let errorCount = $error.text()
      cy.log(errorCount)

      if (parseInt(errorCount) != 0) {
        cy.log('There are errors in the mandatory feilds for your sections')
        expect(parseInt(errorCount)).to.equal(0)
      }
    })

  })
  //UserStory 3: AC2 will fail due to assertion error, Label error has to be 0 but actual is 5.

  it('UserStory 3: AC1', () => {

    /*
  AC 1: Upon filling all mandatory inputs in all 6 form sections and clicking the ‘Review’ button
  in the ‘Declare and Review’ section, the Applicant should be taken to a read-only summary page.
  */


    cy.get(Locators.My_Grants).first().click()
    
    cy.get('a[href$="#drafts"]').click()
    
    cy.get('#db-apps-drafts > .tab-content > :nth-child(1) > .project-title > a > .title-div').click()
    cy.get(Locators.Proceed).click()
    cy.get('a[href$="declaration"]').click()
    
    //Eligibiliy
    cy.get('a[href$="/form/eligibility"]').click()
    //cy.get('[type="radio"][value="true"]').check() This is failing for some reason, cypress finds 9 objects but actuls is 4
    cy.get('#react-eligibility-sg_registered_check-true').should('be.visible').click()
    cy.get('#react-eligibility-turnover_check-true').should('be.visible').click()
    cy.get('#react-eligibility-global_hq_check-true').should('be.visible').click()
    cy.get('#react-eligibility-new_target_market_check-true').should('be.visible').click()
    cy.get('#react-eligibility-started_project_check-true').should('be.visible').click()
   
    cy.get(Locators.SaveBtn)
    
    /*Contact Info */

       cy.get('a[href$="/form/contact_info"]').first().click()
        cy.get('.subsection-title').contains('Main Contact Person')  //verifiying presence of subsection

        cy.get(Locators.Contact_Info_Content).contains('Name')  // Verified Page Contains Name
        cy.get(Locators.Contact_Info_Name_Id).clear().type(Locators.Contact_Info_Name) //If already existing, clearing and Typing new Name
        cy.log('New Name has been added in the Name field')

        cy.get(Locators.Job_Title_Content).contains('Job Title')// Verified Page Contains Job Title
        cy.get(Locators.Job_Title).clear().type('Software Engineer')  //Job Title

        cy.get(Locators.Contact_Phone_Content).contains('Contact No.')// Verified Page Contains Contact No
        cy.get(Locators.Contact_Phone).clear().type('86470580')  //Contact Title

        cy.get(Locators.Primary_Email_Content).contains('Email')// Verified Page Contains Email
        cy.get(Locators.Primary_Email_id).clear().type('Customer1@email.com')  //Email

        cy.get(Locators.Secondary_Email_Content).contains('Email')// Verified Page Contains Alternate Contact Person’s Email
        cy.get(Locators.Secondary_Email_Id).clear().type('Customer2@email.com')  //Alternate Contact Person's Email


        // Mailing Address-verify the check box is checked or not in the contact details
        cy.get(Locators.Mailing_Address_CheckBox_Content).contains('Same as registered address in Company Profile')// Verified Page Contains Mailing Address
         // Mailing Address-verify the check box is checked or not in the contact details
         cy.get('#react-contact_info-correspondence_address-copied').check()
               
         // Same as main contact person-verify the check box is checked or not in the contact details
        cy.get('#react-contact_info-copied').check()
        
        cy.get('#save-btn').click()  //save details
        cy.get(Locators.nextpage).click() // next
        

    /*Proposal Details Page */
    
    cy.get(Locators.Project_Title).clear().type('Construct Hospital and School')  //Project Title
    cy.get(Locators.Project_Start_Date).clear().type('31 Aug 2020')  //Start Date
    cy.get(Locators.Project_End_Date).clear().type('01 Feb 2021')  //End Date
    cy.get(Locators.Project_Description).clear().type('The house making for selling purpose')  //Project Description

    cy.get(Locators.Project_Activity_Value).click()
    cy.get(Locators.Project_Activity_Value).type('Market Entry')
    cy.get(Locators.Project_Activity_Value).type('{enter}')   // Activity - Dropdown selection


    cy.get(Locators.Target_Market).click()
    cy.get(Locators.Target_Market).type('Afghanistan')
    cy.get(Locators.Target_Market).type('{enter}')   // Target Market - Dropdown selection


    cy.get(Locators.Entity_Type).clear().type('wholly owned foreign entity') // Type of Entity
    cy.get(Locators.Share_Holding_Percent).clear().type('30') // Percentage Shareholdings
    cy.get(Locators.Target_Outside).check() // target market outside Singapore
    cy.get(Locators.Remarks).type('Yes all done! Please proceed') // Remarks 

    // next
    cy.get(Locators.SaveBtn).click()  //save details  \
    cy.get(Locators.nextpage).click()
    // cy.get('a[href$="project_impact"]').click({ multiple: true })  //goes to the review page  


    /*Business Impact Page */
    cy.get(Locators.FY_End_Date).clear().type('01 Dec 2020')  //FY End date
    cy.get(Locators.Overseas_Sales_0).clear().type('1200')  //Overseas Sales- First Year
    cy.get(Locators.Overseas_Sales_1).clear().type('1300')  //Overseas Sales- Second Year
    cy.get(Locators.Overseas_Sales_2).clear().type('1400')  //Overseas Sales- Third Year
    cy.get(Locators.Overseas_Sales_3).clear().type('1500')  //Overseas Sales- Fourth Year
    cy.get(Locators.Overseas_Investment_0).clear().type('1200')  //Overseas Investment- First Year
    cy.get(Locators.Overseas_Investment_1).clear().type('1300')  //Overseas Investment- Second Year
    cy.get(Locators.Overseas_Investment_2).clear().type('1400')  //Overseas Investment - Third Year
    cy.get(Locators.Overseas_Investment_3).clear().type('1500')  //Overseas Investment- Fourth Year   
    cy.get(Locators.Rationale_Remarks).clear().type('Will need to develop both short- and mid-term financial projections')  //Rationale of projections
    cy.get(Locators.Benefits_Remarks).clear().type('I agree with non tangiable banefits')  //Non Tangiable Benefits

    cy.get(Locators.SaveBtn).click()
    cy.get(Locators.nextpage).click()


    /*Cost */
    cy.get('#react-project_cost-vendors-accordion-header').click()   // Additem to Third part Header(select box)
   
    cy.get('#react-project_cost-vendors-add-item').click() // Add item Form
    
    cy.get('#react-project_cost-vendors-0-local_vendor-false').check()
    cy.get('#react-project_cost-vendors-0-vendor_name').type('Li su pang',{force: true})  // Adding name of the Vendor
    cy.get('#react-project_cost-vendors-0-amount_in_billing_currency').type('5000',{force: true})  //Estimated Cost in Billing Currency
      cy.get('#react-project_cost-remarks').type('Remarks are added succefully',{force: true})  // Adding Remark
         /* Drag-n-drop component */
     cy.get('#react-project_cost-vendors-0-attachments-btn').attachFile('sample.png', { subjectType: 'drag-n-drop' }); 

         // next
      cy.get('#save-btn').click()  //save details
      cy.get(Locators.nextpage).click()
   
   
   
    /* Declare and Review */

    // if not checked-
    cy.get(Locators.Criminal_Liability_Radio_OFF).should('be.visible').should('not.be.checked').click()
    cy.get(Locators.Civil_Proceeding_Radio_OFF).should('be.visible').should('not.be.checked').click()
    cy.get(Locators.Insolvency_Proceeding_OFF).should('be.visible').should('not.be.checked').click()
    cy.get(Locators.Project_Incentives_OFF).should('be.visible').should('not.be.checked').click()
    cy.get(Locators.Other_Incentives_OFF).should('be.visible').should('not.be.checked').click()
    cy.get(Locators.Project_Commence_OFF).should('be.visible').should('not.be.checked').click()
    cy.get(Locators.Related_Party_OFF).should('be.visible').should('not.be.checked').click()
    cy.get(Locators.COVID_Safe_ON).should('be.visible').should('not.be.checked').click()
    cy.get(Locators.COVID_Safe_Question_ON).should('be.visible').should('not.be.checked').click()
    //**********Consent & Acknowledgement**********//
    
    cy.get(Locators.Consent_Acknowledgment_Check).check().and('have.value', 'on')
    cy.get(Locators.SaveBtn).click()
    cy.get(Locators.ReviewBtn).click()



  })
  
// UserStory 3: AC1 is passed- credentials on all sections will be succefully enetered
  /*
  AC 3: The read-only summary page should correctly contain all the details previously filled in each form section.
  */
  it('UserStory 3: AC3', () => {

    cy.get(Locators.My_Grants).first().click()
    
    cy.get('a[href$="#drafts"]').click()
   
    cy.get('#db-apps-drafts > .tab-content > :nth-child(1) > .project-title > a > .title-div').click()
    cy.get(Locators.Proceed).click()
    //checks whether the eligibility section selection is same as before
    cy.get('[id^="react-eligibility"]').each(($el, index, $list) => {
      expect($el.text()).to.equal('Yes')
    })

    cy.get('[id^="react-contact_info"].bgp-readonly').each(($el, index, $list) => {
      expect($el.text()).to.not.be.empty
    })

    cy.get('[id^="react-project"].bgp-readonly').each(($el, index, $list) => {

      expect($el.text()).to.not.be.empty
    })

    cy.get('[id^="react-project_impact"].bgp-readonly').each(($el, index, $list) => {

      expect($el.text()).to.not.be.empty
    })
    cy.get('[id^="react-project_cost"].bgp-readonly').each(($el, index, $list) => {

      expect($el.text()).to.not.be.empty
    })
    // This css selector will fetch all the elements having both below id and class
    cy.get('[id^="react-declaration"].bgp-radio-readonly').each(($el, index, $list) => {

      if (index > 6) { expect($el.text()).to.equal('Yes') }
      else { expect($el.text()).to.equal('No') }
    })

  // This UserStory 3: AC3 is passed

  })
// 
  /*
  AC 4: At the bottom of the read-only summary page is a final ‘Consent and Acknowledgement’ checkbox.
  */
  it('UserStory 3: AC4', () => {

    cy.get('#grants > :nth-child(3) > .dashboard-tab-container > .nav > :nth-child(2) > a').click()
    cy.get('#db-apps-drafts > .tab-content > :nth-child(1) > .project-title > a > .title-div').click()
    cy.get('#keyPage-form-button').click()

    cy.scrollTo('bottom')
  
    cy.get('#react-declaration-info_truthfulness_check').should('be.visible')


  })
 // UserStory 3: AC4' is passed
  /*
  AC 5: Once checked, the Applicant can submit the entire Application and a Success message box should be shown. The Success message box should contain an Application Ref ID and 
  Agency details should display the receiving Agency as ‘Enterprise Singapore’.
  */
  it('UserStory 3: AC5', () => {
    cy.get('#grants > :nth-child(3) > .dashboard-tab-container > .nav > :nth-child(2) > a').click()
      cy.get('#db-apps-drafts > .tab-content > :nth-child(1) > .project-title > a > .title-div').click()
      cy.get('#keyPage-form-button').click()
      cy.scrollTo('bottom')
      cy.get('#react-declaration-info_truthfulness_check').should('be.visible').check()
      
      cy.get('#submit-btn').click()
     
      cy.scrollTo('top') 

     cy.get('.value').then(($el) => {
      
        expect($el.eq(0)).not.to.be.empty
        expect($el.eq(3)).to.contain('Enterprise Singapore')
       
       
        cy.log($el.eq(0).text())
        Locators.RefID = $el.eq(0).text()
      cy.log(Locators.RefID)  

      })

      cy.get('.card').contains('Ref ID:')
  })
  /*
  AC 6: Upon submission, the main ‘My Grants’ dashboard should show the Application under the ‘Processing’ tab.
  */
  it('UserStory 3: AC6', () => {
    cy.get('a[href*="#processing"]').click()
    cy.get('#processing .tab-content').should('contain', Locators.RefID)

  })
// UserStory 3: AC6' is passed


})
