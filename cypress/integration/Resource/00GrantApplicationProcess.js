import Locators from '../PageObjects/Locators.json'
Cypress.config({'pageLoadTimeout':100000,'defaultCommandTimeout':15000})

describe('Test Business Grants Portal - New Grant Automation Flow', () => {

  beforeEach('Testing the Window Prompt with Credentials', () => {
    
    // Created Customized Command for Authorize and Login
    cy.AuthorizeBGP(Locators.Authorize_username,Locators.Authorize_password)
    cy.logintoBGP(Locators.CPUID,Locators.CPUID_Fullname,Locators.CPENT_ID)

    // Verified the Title of the Page (Functional Testing)
    cy.title().should('eq', 'Business Grants Portal')
    cy.get(Locators.My_Grants).first().click()
 
  
    //Get a New Grant
    cy.GetNewGrant()
    // Here create a sample Grant,which can get in Drafts
    cy.CreateSampleGrant() 
  })
})