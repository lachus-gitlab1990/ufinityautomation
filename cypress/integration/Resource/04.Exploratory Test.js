import Locators from '../PageObjects/Locators.json';

describe('Test Business Grants Portal - New Grant Automation Flow', () => {
    
    it('Testing the Window Prompt with Credentials', ()=> {
        let username1 = "public"
        let password1 = "Let$BeC001"
         cy.visit({
	     url: 'https://bgp-qa.gds-gov.tech/',
	     auth: {
            username: username1,
            password: password1
          }
        })}) ;
        
        //login page with credentials
    
    it('Test the Registration form Login with test Credentials', ()=>{    
        cy.get(Locators.LoginBtn).click()
        cy.get('[name="CPUID"]').clear().type('S1234567A')
        cy.get('[name="CPUID_FullName"]').clear().type('Tan Ah Kow')
        cy.get('[name="CPEntID"]').clear().type('BGPQEDEMO')
        cy.get('[name="CPEntID"]').clear().type('Acceptor')
        cy.get('[type="submit"]').last().click()
        cy.wait(1000)
    })

    it('1.Eligibility' , ()=> {
        cy.get('[id="login-button"]').click()
        cy.get('[name="CPUID"]').clear().type('S1234567A')
        cy.get('[name="CPUID_FullName"]').clear().type('Tan Ah Kow')
        cy.get('[name="CPEntID"]').clear().type('BGPQEDEMO')
        cy.get('[name="CPEntID"]').clear().type('Acceptor')
        cy.get('[type="submit"]').last().click()
        cy.get('[id="sgds-nav-start"]').first().click()
        cy.get('#grants > :nth-child(3) > .dashboard-tab-container > .nav > :nth-child(2) > a').click()
        cy.get(':nth-child(1) > .project-title > a > .title-div').click()
        cy.get('#keyPage-form-button').click()

    // validation on visibility and checked(Positive and negative scenarios)
     cy.get('#react-eligibility-sg_registered_check-true').should('be.visible').should('not.be.checked')
     cy.get('#react-eligibility-sg_registered_check-false').should('be.visible').should('not.be.checked')
        
     // if not checked-

     cy.get('#react-eligibility-sg_registered_check-true').should('be.visible').should('not.be.checked').click()

      
    // validation on visibility and checked(Positive and negative scenarios)
     cy.get('#react-eligibility-turnover_check-true').should('be.visible').should('not.be.checked')
     cy.get('#react-eligibility-turnover_check-false').should('be.visible').should('not.be.checked')
        
              // if not checked-

     cy.get('#react-eligibility-turnover_check-true').should('be.visible').should('not.be.checked').click()
               
     // validation on visibility and checked(Positive and negative scenarios)
     cy.get('#react-eligibility-global_hq_check-true').should('be.visible').should('not.be.checked')
     cy.get('#react-eligibility-global_hq_check-false').should('be.visible').should('not.be.checked')
        
              // if not checked-

     cy.get('#react-eligibility-global_hq_check-true').should('be.visible').should('not.be.checked').click()
               
     // validation on visibility and checked(Positive and negative scenarios)
     cy.get('#react-eligibility-new_target_market_check-true').should('be.visible').should('not.be.checked')
     cy.get('#react-eligibility-new_target_market_check-false').should('be.visible').should('not.be.checked')
        
              // if not checked-

     cy.get('#react-eligibility-new_target_market_check-true').should('be.visible').should('not.be.checked').click()
           
    // validation on visibility and checked(Positive and negative scenarios)- Radio Buttons
    cy.get('#react-eligibility-started_project_check-true').should('be.visible').should('not.be.checked')
    cy.get('#react-eligibility-started_project_check-false').should('be.visible').should('not.be.checked')
   
     // if not checked-

    cy.get('#react-eligibility-started_project_check-true').should('be.visible').should('not.be.checked').click()          
 
     
     // next

     cy.get(Locators.nextpage).click()
    })

    it('2.Contact Details' , ()=> {
        cy.get('[id="login-button"]').click()
        cy.get('[name="CPUID"]').clear().type('S1234567A')
        cy.get('[name="CPUID_FullName"]').clear().type('Tan Ah Kow')
        cy.get('[name="CPEntID"]').clear().type('BGPQEDEMO')
        cy.get('[name="CPEntID"]').clear().type('Acceptor')
        cy.get('[type="submit"]').last().click()
        cy.get('[id="sgds-nav-start"]').first().click()
        cy.get('#grants > :nth-child(3) > .dashboard-tab-container > .nav > :nth-child(2) > a').click()
        cy.get(':nth-child(1) > .project-title > a > .title-div').click()
        cy.get('#keyPage-form-button').click()
        // next
        cy.get(Locators.nextpage).click()
        
        /*Contact Details Page */
        cy.get('#react-contact_info-name').clear().type('Customer1')  //Name
        cy.get('#react-contact_info-designation').clear().type('Software Engineer')  //Job Title
        cy.get('#react-contact_info-phone').clear().type('86470580')  //Contact Title
        cy.get('#react-contact_info-primary_email').clear().type('Customer1@email.com')  //Email
        cy.get('#react-contact_info-secondary_email').clear().type('Customer2@email.com')  //Alternate Contact Person's Email

         // Mailing Address-verify the check box is checked or not in the contact details
        cy.get('#react-contact_info-correspondence_address-copied').check().and('have.value','on')
               
         // Same as main contact person-verify the check box is checked or not in the contact details
        cy.get('#react-contact_info-copied').check().and('have.value','on')

        
        cy.get('#save-btn').click()  //save details
        cy.get(Locators.nextpage).click() // next
        
    })
        ///Proposal Details

    it('3.Proposal Details' , ()=> {
        cy.get('[id="login-button"]').click()
        cy.get('[name="CPUID"]').clear().type('S1234567A')
        cy.get('[name="CPUID_FullName"]').clear().type('Tan Ah Kow')
        cy.get('[name="CPEntID"]').clear().type('BGPQEDEMO')
        cy.get('[name="CPEntID"]').clear().type('Acceptor')
        cy.get('[type="submit"]').last().click()
        cy.get('[id="sgds-nav-start"]').first().click()
        cy.get('#grants > :nth-child(3) > .dashboard-tab-container > .nav > :nth-child(2) > a').click()
        cy.get(':nth-child(1) > .project-title > a > .title-div').click()
        cy.get('#keyPage-form-button').click()
            
        cy.get(Locators.nextpage).click() // next to Contact Details <-- Eligibilty Page
        cy.wait(5000)
        cy.get(Locators.nextpage).click() // next to Proposal Details <-- Contact Details  
        
        

            
            /*Proposal Details Page */
        cy.get('#react-project-title').clear().type('Construct Hospital')  //Project Title
        cy.get('#react-project-start_date').clear().type('01 Aug 2020')  //Start Date
        cy.get('#react-project-end_date').clear().type('01 Feb 2021')  //End Date
        cy.get('#react-project-description').clear().type('The house making for selling purpose')  //Project Description
     
        cy.get('#react-select-project-activity--value').click()
        cy.get('.Select-multi-value-wrapper[id="react-select-project-activity--value"]').type('Market Entry')
        cy.get('.Select-multi-value-wrapper[id="react-select-project-activity--value"]').type('{enter}')   // Activity - Dropdown selection
     

        cy.get('#react-select-project-primary_market--value').click()
        cy.get('#react-select-project-primary_market--value').type('Afghanistan')
        cy.get('#react-select-project-primary_market--value').type('{enter}')   // Target Market - Dropdown selection
     
     
        cy.get('#react-project-entity_type').clear().type('wholly owned foreign entity') // Type of Entity
        cy.get('#react-project-shareholding_percentage').clear().type('30') // Percentage Shareholdings
        cy.get('#react-project-is_first_time_expand-true').check() // target market outside Singapore
        cy.get('#react-project-remarks').type('Yes all done! Please proceed') // Remarks
     
           // next
        cy.get('#save-btn').click()  //save details
        cy.get(Locators.nextpage).click() // next
     
        
    
    })

    /* Business project */

    it('4.Business project' , ()=> {
        cy.get('[id="login-button"]').click()
        cy.get('[name="CPUID"]').clear().type('S1234567A')
        cy.get('[name="CPUID_FullName"]').clear().type('Tan Ah Kow')
        cy.get('[name="CPEntID"]').clear().type('BGPQEDEMO')
        cy.get('[name="CPEntID"]').clear().type('Acceptor')
        cy.get('[type="submit"]').last().click()
        cy.get('[id="sgds-nav-start"]').first().click()
        cy.get('#grants > :nth-child(3) > .dashboard-tab-container > .nav > :nth-child(2) > a').click()
        cy.get(':nth-child(1) > .project-title > a > .title-div').click()
        cy.get('#keyPage-form-button').click()
            
        cy.get(Locators.nextpage).click() // next to Contact Details <-- Eligibilty Page
        cy.wait(2000)
        cy.get(Locators.nextpage).click()// next to Proposal Details <-- Contact Details 
        cy.wait(2000)
        cy.get(Locators.nextpage).click()// next to Business Impact<-- Proposal Details
        

            
            /*Business Impact Page */
        cy.get('#react-project_impact-fy_end_date_0').clear().type('01 Dec 2020')  //FY End date
        cy.get('#react-project_impact-overseas_sales_0').clear().type('1200')  //Overseas Sales- First Year
        cy.get('#react-project_impact-overseas_sales_1').clear().type('1300')  //Overseas Sales- Second Year
        cy.get('#react-project_impact-overseas_sales_2').clear().type('1400')  //Overseas Sales- Third Year
        cy.get('#react-project_impact-overseas_sales_3').clear().type('1500')  //Overseas Sales- Fourth Year
        cy.get('#react-project_impact-overseas_investments_0').clear().type('1200')  //Overseas Investment- First Year
        cy.get('#react-project_impact-overseas_investments_1').clear().type('1300')  //Overseas Investment- Second Year
        cy.get('#react-project_impact-overseas_investments_2').clear().type('1400')  //Overseas Investment - Third Year
        cy.get('#react-project_impact-overseas_investments_3').clear().type('1500')  //Overseas Investment- Fourth Year   
        cy.get('#react-project_impact-rationale_remarks').clear().type('Will need to develop both short- and mid-term financial projections')  //Rationale of projections
        cy.get('#react-project_impact-benefits_remarks').clear().type('I agree with non tangiable banefits')  //Non Tangiable Benefits
            
           // next
        cy.get('#save-btn').click()  //save details
        cy.get(Locators.nextpage).click() // next 
    
    })

    /* Cost */

    it.only('5.Cost' , ()=> {
        cy.get('[id="login-button"]').click()
        cy.get('[name="CPUID"]').clear().type('S1234567A')
        cy.get('[name="CPUID_FullName"]').clear().type('Tan Ah Kow')
        cy.get('[name="CPEntID"]').clear().type('BGPQEDEMO')
        cy.get('[name="CPEntID"]').clear().type('Acceptor')
        cy.get('[type="submit"]').last().click()
        cy.get('[id="sgds-nav-start"]').first().click()
        cy.get('#grants > :nth-child(3) > .dashboard-tab-container > .nav > :nth-child(2) > a').click()
        cy.get(':nth-child(1) > .project-title > a > .title-div').click()
        cy.get('#keyPage-form-button').click()
            
        cy.get(Locators.nextpage).click() // next to Contact Details <-- Eligibilty Page
        cy.wait(2000)
        cy.get(Locators.nextpage).click() // next to Personal Details <-- Contact Details  
        cy.wait(2000)
        cy.get(Locators.nextpage).click() // next to Business Impact<-- Personal Details
        cy.wait(2000)
        cy.get(Locators.nextpage).click() // next to Cost <-- Business Impact
        cy.wait(2000)


            
            /*Cost */
        cy.get('#react-project_cost-vendors-accordion-header').click()   // Additem to Third part Header(select box)
        cy.get('#react-project_cost-vendors-add-item').click() // Add item Form
            // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
        cy.get('#react-project_cost-vendors-0-local_vendor-true').should('be.visible').should('not.be.checked')
        cy.get('#react-project_cost-vendors-0-local_vendor-false').should('be.visible').should('not.be.checked')
          
            // if not checked-
       
        cy.get('#react-project_cost-vendors-0-local_vendor-false').should('be.visible').should('not.be.checked').click()    // Registered in Singapore
        cy.get('#react-project_cost-vendors-0-vendor_name').type('Li su pang')  // Adding name of the Vendor
            
           //Attchemeny document (not written)
        cy.get('#react-project_cost-vendors-0-amount_in_billing_currency').type('5000')  //Estimated Cost in Billing Currency
        cy.get('#react-project_cost-remarks').type('Remarks are added succefully')  // Adding Remark
           /* Drag-n-drop component */
        cy.get('#react-project_cost-vendors-0-attachments-btn').attachFile('sample.png', { subjectType: 'drag-n-drop' });

           // next
        cy.get('#save-btn').click()  //save details
        cy.get(Locators.nextpage).click() // next 
    
    })

    /* Declare and Review */

    it.only('6.Declare and Review' , ()=> {
        cy.get('[id="login-button"]').click()
        cy.get('[name="CPUID"]').clear().type('S1234567A')
        cy.get('[name="CPUID_FullName"]').clear().type('Tan Ah Kow')
        cy.get('[name="CPEntID"]').clear().type('BGPQEDEMO')
        cy.get('[name="CPEntID"]').clear().type('Acceptor')
        cy.get('[type="submit"]').last().click()
        cy.get('[id="sgds-nav-start"]').first().click()
        cy.get('#grants > :nth-child(3) > .dashboard-tab-container > .nav > :nth-child(2) > a').click()
        cy.get(':nth-child(1) > .project-title > a > .title-div').click()
        cy.get('#keyPage-form-button').click()
            
        cy.get(Locators.nextpage).click() // next to Contact Details <-- Eligibilty Page
        cy.wait(2000)
        cy.get(Locators.nextpage).click() // next to Personal Details <-- Contact Details  
        cy.wait(2000)
        cy.get(Locators.nextpage).click() // next to Business Impact<-- Personal Details
        cy.wait(2000)
        cy.get(Locators.nextpage).click() // next to Cost <-- Business Impact
        cy.wait(2000)


            
            /* Declare and Review */
        // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
        cy.get('#react-declaration-criminal_liability_check-true').should('be.visible').should('not.be.checked')
        cy.get('#react-declaration-criminal_liability_check-false').should('be.visible').should('not.be.checked')
        // if not checked-
        cy.get('#react-declaration-criminal_liability_check-false').should('be.visible').should('not.be.checked').click()    // 1,charged with or convicted of any criminal offence 
        // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
        cy.get('#react-declaration-civil_proceeding_check-true').should('be.visible').should('not.be.checked')
        cy.get('#react-declaration-civil_proceeding_check-false').should('be.visible').should('not.be.checked')
         // if not checked-
        cy.get('#react-declaration-civil_proceeding_check-false').should('be.visible').should('not.be.checked').click()    // 2,engaged in any civil suit or proceedings
      
        // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
        cy.get('#react-declaration-insolvency_proceeding_check-false').should('be.visible').should('not.be.checked')
        cy.get('#react-declaration-insolvency_proceeding_check-true').should('be.visible').should('not.be.checked')
        // if not checked-
        cy.get('#react-declaration-insolvency_proceeding_check-false').should('be.visible').should('not.be.checked').click()    // 3,bankrupt     
       // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
        cy.get('#react-declaration-project_incentives_check-false').should('be.visible').should('not.be.checked')
        cy.get('#react-declaration-project_incentives_check-true').should('be.visible').should('not.be.checked')
        // if not checked-
        cy.get('#react-declaration-project_incentives_check-false').should('be.visible').should('not.be.checked').click()    // 4,applicant applied for or obtained any other grants or tax or financial incentives
        // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
       cy.get('#react-declaration-other_incentives_check-false').should('be.visible').should('not.be.checked')
       cy.get('#react-declaration-other_incentives_check-true').should('be.visible').should('not.be.checked')
       // if not checked-
       cy.get('#react-declaration-other_incentives_check-false').should('be.visible').should('not.be.checked').click()    // 5,obtained any other incentives or concessions, e.g. Pioneer Incentive,
       // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
       cy.get('#react-declaration-project_commence_check-false').should('be.visible').should('not.be.checked')
       cy.get('#react-declaration-project_commence_check-true').should('be.visible').should('not.be.checked')
       // if not checked-
       cy.get('#react-declaration-project_commence_check-false').should('be.visible').should('not.be.checked').click()    // 6,applicant commenced on the project prior to this application
       // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
       cy.get('#react-declaration-related_party_check-false').should('be.visible').should('not.be.checked')
       cy.get('#react-declaration-related_party_check-true').should('be.visible').should('not.be.checked')
       // if not checked-
       cy.get('#react-declaration-related_party_check-false').should('be.visible').should('not.be.checked').click()    // 7,suppliers and service providers engaged in this project have any relationship
       // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
       cy.get('#react-declaration-covid_safe_check-false').should('be.visible').should('not.be.checked')
       cy.get('#react-declaration-covid_safe_check-true').should('be.visible').should('not.be.checked')
       // if not checked-
       cy.get('#react-declaration-covid_safe_check-true').should('be.visible').should('not.be.checked').click()    // 8,Applicant has complied with all applicable safe distancing and other health measure
       // validation on visibility and checked(Positive and negative scenarios on Radio button check boxes)
       cy.get('#react-declaration-covid_safe_ques_check-false').should('be.visible').should('not.be.checked')
       cy.get('#react-declaration-covid_safe_ques_check-true').should('be.visible').should('not.be.checked')
       // if not checked-
       cy.get('#react-declaration-covid_safe_ques_check-true').should('be.visible').should('not.be.checked').click()    // 9.  Applicant agrees to comply with all applicable SDM

     //**********Consent & Acknowledgement**********//
       cy.get('react-declaration-consent_acknowledgement_check').check().and('have.value','on')
       cy.get('#review-btn').click() // Review Button

    })
       
   
});