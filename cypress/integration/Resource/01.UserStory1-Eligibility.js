import Locators from '../PageObjects/Locators.json'
Cypress.config({'pageLoadTimeout':100000,'defaultCommandTimeout':15000})

describe('Test Business Grants Portal - New Grant Automation Flow', () => {

  beforeEach('Testing the Window Prompt with Credentials', () => {
    
    // Created Customized Command for Authorize and Login
    cy.AuthorizeBGP(Locators.Authorize_username,Locators.Authorize_password)
    cy.logintoBGP(Locators.CPUID,Locators.CPUID_Fullname,Locators.CPENT_ID)

    // Verified the Title of the Page (Functional Testing)
    cy.title().should('eq', 'Business Grants Portal')
    cy.get(Locators.My_Grants).first().click()
 
  
    //Get a New Grant
    cy.GetNewGrant()
    // Here create a sample Grant,which can get in Drafts
    cy.CreateSampleGrant() 
  })

  it('User Story 1 – Eligibility Section -AC1', () => {

    //AC 1: The section should contain 4 questions:

    //Verify if the section only contains 4 questions
    //Get the New Grant Created from Drafts
    cy.FindProfile()
    cy.contains('* Mandatory field').should('be.visible')
    cy.get('.control-label').should(($lis) => {
      expect($lis.eq(0), 'First item').to.contain('Is the applicant registered in Singapore?')
      expect($lis.eq(1), 'Second item').to.contain("Is the applicant's group sales turnover less than or equal to S$100m or is the applicant's group employment size less than or equal to 200?")
      expect($lis.eq(2), 'Third item').to.contain('Does the applicant have at least 30%')
      expect($lis.eq(3), 'Fourth item').to.contain('Are all the following statements true for this project?')
      expect($lis).to.have.length(4)

    })
  })
  // User Story 1 – Eligibility Section -AC1 will fail as the expected is 4 questions per Requirement Doc and actual is 5

  it('User Story 1 – Eligibility Section -AC2', () => {

    //Get the New Grant Created from Drafts
    cy.FindProfile()
    cy.get('.control-label').each(($el, index, $list) => {
    cy.wrap($el).should('be.visible').log($el.text())
    })
    cy.get('[type="radio"]').check('true').should('be.checked')
    cy.get('[type="radio"]').check('false').should('be.checked')
  })
 // 'User Story 1 – Eligibility Section -AC2' will pass as Radio buttons are successfully checkable
  it('User Story 1 – Eligibility Section -AC3', () => {

    //Get the New Grant Created from Drafts
    cy.FindProfile()
    cy.get('[type="radio"][value="false"]').check()
    cy.get('.eligibility-advice').each(($el, index, $list) => {
      expect($el).to.contain('Visit Smart Advisor on the SME Portal for more information on other government assistance.').should('be.visible')
    })
  })
/* User Story 1 – Eligibility Section -AC3 will fail as actual is "The applicant may not meet the eligibility criteria for this grant. 
Visit FAQ page for more information on other government grants." and expected is "Visit Smart Advisor on the SME Portal for more 
information on other government assistance." as per the requirement document */

  it('User Story 1 – Eligibility Section -AC4', () => {

    //Get the New Grant Created from Drafts
    cy.FindProfile()
    cy.get('[type="radio"][value="false"]').check()
    cy.get('a[href^="https"]').each(($el, index, $list) => {
      expect($el).to.contain('https://www.smeportal.sg/content/smeportal/en/moneymatters.html#saText')
    })

  })
  /* User Story 1 – Eligibility Section -AC4 will fail as there is no link in the warning message showing in actual result" */

  it('User Story 1 – Eligibility Section -AC5', () => {

    //Get the New Grant Created from Drafts
    cy.FindProfile()
    cy.get('[type="radio"][value="true"]').check()
    cy.get('[type="radio"]:checked').then(($el) => {
    cy.get(Locators.SaveBtn).click()
    cy.reload()
    cy.get('[type="radio"]:checked').should('have.value', 'true')

    })
  });
 // User Story 1 – Eligibility Section -AC5 will pass as the saved items and reloaded items are same 
});