import Locators from '../PageObjects/Locators.json'
Cypress.config({'pageLoadTimeout':100000,'defaultCommandTimeout':15000})

describe('Test Business Grants Portal - New Grant Automation Flow', () => {

  beforeEach('Testing the Window Prompt with Credentials', () => {
    
    // Created Customized Command for Authorize and Login
    cy.AuthorizeBGP(Locators.Authorize_username,Locators.Authorize_password)
    cy.logintoBGP(Locators.CPUID,Locators.CPUID_Fullname,Locators.CPENT_ID)

    // Verified the Title of the Page (Functional Testing)
    cy.title().should('eq', 'Business Grants Portal')
    cy.get(Locators.My_Grants).first().click()

    //Get a New Grant
    cy.GetNewGrant()
    // Here create a sample Grant,which can get in Drafts
    cy.CreateSampleGrant() 
  })
    
    it('UserStory 2 - Contact Section -AC1', () => {
        /*Contact Details Page 
    
        AC 1: The page contains a ‘Main Contact Person’ subsection with the following inputs:
          •	Name
          •	Job Title
          •	Contact No
          •	Email
          •	Alternate Contact Person’s Email
          •	Mailing Address

        */
        cy.FindProfile()
        cy.get('a[href$="/form/contact_info"]').first().click()
        cy.get('.subsection-title').contains('Main Contact Person')  //verifiying presence of subsection

        cy.get(Locators.Contact_Info_Content).contains('Name')  // Verified Page Contains Name
        cy.get(Locators.Contact_Info_Name_Id).clear().type(Locators.Contact_Info_Name) //If already existing, clearing and Typing new Name
        cy.log('New Name has been added in the Name field')

        cy.get(Locators.Job_Title_Content).contains('Job Title')// Verified Page Contains Job Title
        cy.get(Locators.Job_Title).clear().type('Software Engineer')  //Job Title

        cy.get(Locators.Contact_Phone_Content).contains('Contact No.')// Verified Page Contains Contact No
        cy.get(Locators.Contact_Phone).clear().type('86470580')  //Contact Title

        cy.get(Locators.Primary_Email_Content).contains('Email')// Verified Page Contains Email
        cy.get(Locators.Primary_Email_id).clear().type('Customer1@email.com')  //Email

        cy.get(Locators.Secondary_Email_Content).contains('Email')// Verified Page Contains Alternate Contact Person’s Email
        cy.get(Locators.Secondary_Email_Id).clear().type('Customer2@email.com')  //Alternate Contact Person's Email


        // Mailing Address-verify the check box is checked or not in the contact details
        cy.get(Locators.Mailing_Address_CheckBox_Content).contains('Same as registered address in Company Profile')// Verified Page Contains Mailing Address


    })
  // UserStory 2 - Contact Section -AC1 will pass , as credentials can be successfully entered


    it ('UserStory 2 - Contact Section -AC2', () => {
        
       cy.FindProfile()
       cy.get(Locators.Contact_Info_Page).first().click()
      
       cy.get(Locators.Mailing_Address_CheckBox).uncheck()
       cy.get(Locators.PostaCode_id).clear().type('544269')
       
       cy.get('#react-contact_info-correspondence_address-block').then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
        cy.get('#react-contact_info-correspondence_address-street').then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })

    })
    /*
        UserStory 2 - Contact Section -AC2 will pass , as the script is checking the block 
        and street feilds are not empty Api response on autopopulating address with entered postal code
    */

    it ('UserStory 2 - Contact Section -AC3', () => {
        
       cy.FindProfile()
       cy.get(Locators.Contact_Info_Page).first().click()
       cy.get(Locators.Mailing_Address_CheckBox).check().and('have.value', 'on')
        
        cy.get('#react-contact_info-correspondence_address-block').then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
        cy.get('#react-contact_info-correspondence_address-street').then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
        cy.get(Locators.Mailing_Address_Postal_Code).then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
    
    })
    /*
        UserStory 2 - Contact Section -AC3 will pass , as the script is checking
     feilds are not empty when 'Same as registered address in Company Profile' check box is on
    
     */

    it ('UserStory 2 - Contact Section -AC4', () => {
        
       cy.FindProfile()
       cy.get(Locators.Contact_Info_Page).first().click()
       cy.get('.subsection-title').contains('Letter Of Offer Addressee')  //verifiying presence of subsection
       cy.get('#react-contact_info-copied').uncheck()
       cy.get(Locators.Offeree_Name_Id).clear().type(Locators.Offeree_Name).should('have.value',Locators.Offeree_Name)
       cy.get(Locators.Offeree_Job_Title_Id).clear().type(Locators.Offereee_Job_Title).should('have.value',Locators.Offereee_Job_Title)
       cy.get(Locators.Offeree_Email_Id).clear().type(Locators.Offeree_Email).should('have.value',Locators.Offeree_Email)

    })
    /*
        UserStory 2 - Contact Section -AC4 is passed, The page contains a ‘Letter of Offer Addressee’ subsection with the following inputs:
         •	Name
         •	Job Title
         •	Email
        */

    it ('UserStory 2 - Contact Section -AC5', () => {

        
       cy.FindProfile()
       cy.get(Locators.Contact_Info_Page).first().click()
       cy.get('#react-contact_info-copied').check().and('have.value', 'on')
       cy.get(Locators.Contact_Info_Name_Id).then(($el) => { 
            let title = $el.val()
            cy.get(Locators.Offeree_Name_Id).should('have.value',title)
        })

        cy.get(Locators.Job_Title).then(($el) => {
            
            let title = $el.val()
            cy.get(Locators.Offeree_Job_Title_Id).should('have.value',title)
        })
        cy.get(Locators.Primary_Email_id).then(($el) => {
            
            let title = $el.val()
            cy.get(Locators.Offeree_Email_Id).should('have.value',title)
        })
    })
    /*
        UserStory 2 - Contact Section -AC5 is passed as the option ‘ Same as main contact person’ 
        will populate the subsection in AC 4 with details from the ‘Main Contact Person’ in AC 1.
        */

    it('UserStory 2 - Contact Section -AC6', () => {
        
       cy.FindProfile()
       cy.get(Locators.Contact_Info_Page).first().click()
       cy.get(Locators.Contact_Info_Name_Id).clear().type(Locators.Contact_Info_Name)
       cy.get(Locators.Job_Title).clear().type('Software Engineer')
       cy.get(Locators.Contact_Phone).clear().type('86470580') 
       cy.get(Locators.Primary_Email_id).clear().type('Customer1@email.com')
       cy.get(Locators.Secondary_Email_Id).clear().type('Customer2@email.com')
       cy.get(Locators.Mailing_Address_CheckBox).check()
       cy.get('#react-contact_info-copied').check() 
      

        cy.get(Locators.SaveBtn).click()  //save details
        cy.reload() 

        cy.get(Locators.Contact_Info_Name_Id).should('not.be.empty')
        cy.get(Locators.Job_Title).should('not.be.empty')
        cy.get(Locators.Contact_Phone).should('not.be.empty')
        cy.get(Locators.Primary_Email_id).should('not.be.empty')
        cy.get(Locators.Secondary_Email_Id).should('not.be.empty')
        cy.get(Locators.PostaCode_id).should('not.be.empty')
        
        cy.get('#react-contact_info-correspondence_address-block').then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
        cy.get('#react-contact_info-correspondence_address-street').then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
        cy.get(Locators.Mailing_Address_Postal_Code).then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
        
        cy.get(Locators.Offeree_Name_Id).then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
        cy.get(Locators.Offeree_Job_Title_Id).then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
        cy.get(Locators.Offeree_Email_Id).then(($el) => {
            cy.log($el.val())
            cy.wrap($el.val()).should('not.be.empty')
        })
       
    })
    /*
        UserStory 2 - Contact Section -AC6 is passed as Clicking ‘Save’  saves the Applicant’s inputs and 
        refreshing the page reloads the saved values.
     */
});