Feature: Login to BGP portal
  I want to login to the Govtech BGP portal

Scenario: Login to BGP portal
Given I open the lpgin page of BGP portal
When I type the credentials and select the role
And I click Login button
Then Login should be successful