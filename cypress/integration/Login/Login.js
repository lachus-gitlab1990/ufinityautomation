Given('I open the lpgin page of BGP portal' , () => {
    {
        let username1 = "public"
        let password1 = "Let$BeC001"
        cy.visit({
          url: 'https://bgp-qa.gds-gov.tech/',
          auth: {
            username: username1,
            password : password1    
          }
})}});
When('I type the credentials and select the role' , () => {
    cy.get('[id="login-button"]').click()
    cy.get('[name="CPUID"]').clear().type('S1234567A')
    cy.get('[name="CPUID_FullName"]').clear().type('Tan Ah Kow')
    cy.get('[name="CPEntID"]').clear().type('BGPQEDEMO')
    

});

And('I click Login button' , () => {
    cy.get('[type="submit"]').last().click()
});

Then('Login should be successful' , () => {
    cy.title().should('eq', 'Business Grants Portal')
});